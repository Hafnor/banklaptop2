let balance = 0;
let bankBalance = 0;
let loan = 0;
let clickPay = 100;
let laptops = []

//Getting elements
const workButton = document.querySelector('.btn-primary');
const bankButton = document.querySelector('.btn-warning');
const loanButton = document.querySelector('.btn-success');
const payBackLoanButton = document.querySelector('.btn-danger');
const selectLaptops = document.querySelector('.form-select-sm');
const buyLaptop = document.getElementById('laptopBuy');
//Buttons
//Buy laptop
buyLaptop.addEventListener("click", function() {
    var value = selectLaptops.value;
    let tempLaptop = laptops[value-1]
    if (value === "Open this to select laptops") {
        return
    }
    if (tempLaptop.price > bankBalance) {
        alert("Laptop is to expensive");
        return
    }
    bankBalance -= tempLaptop.price
    document.getElementById("bankBalance").innerHTML = bankBalance;
    alert("U own a cool new laptop!!!!!!!!");
  });
//Select laptop
selectLaptops.addEventListener("click", function() {
    var value = selectLaptops.value;
    if (value === "Open this to select laptops") {
        return
    }
    let tempLaptop = laptops[value-1]
    document.getElementById("laptopInfo").innerHTML = tempLaptop.description;
    document.getElementById("laptopTitle").innerHTML = tempLaptop.title;
    document.getElementById("laptopSpecs").innerHTML = tempLaptop.specs;
    document.getElementById("laptopPrice").innerHTML = tempLaptop.price + "kr";
    document.querySelector(".card-img-top").src = "https://noroff-komputer-store-api.herokuapp.com/" + tempLaptop.image

  });

//Work
workButton.addEventListener("click", function() {
    if (loan !== 0) {
        let loanPayment = clickPay *0.1
        let pay = clickPay - loanPayment
        loan -= loanPayment
        balance += pay;
        if(loan < 0) {
            loan = 0
        }
    } else {
        balance += clickPay;
    }
    document.getElementById("balance").innerHTML = balance;
    if (loan <= 0) {
        payBackLoanButton.style.visibility = "hidden"
    }
  });
//Bank
bankButton.addEventListener("click", function() {
    bankBalance += balance
    balance = 0
    document.getElementById("balance").innerHTML = balance;
    document.getElementById("bankBalance").innerHTML = bankBalance;
  });
  //Loan
  loanButton.addEventListener("click", function() {
    if (loan === 0) {
        var bigAssLoan = parseInt(prompt("Enter the loan u want!!!!!!!"), 10);
        if (bankBalance*2 < bigAssLoan) {
            alert("To big my man!");
        }
        else {
            loan = bigAssLoan
            bankBalance+=loan
            document.getElementById("bankBalance").innerHTML = bankBalance;
            payBackLoanButton.style.visibility = "visible"
        }
    }
    else {
        alert(`Pay back the loan of ${loan} first!!!! or u will die broke`);  
    }
    
  });

  payBackLoanButton.addEventListener("click", function() {
    if (bankBalance>=loan) {
        bankBalance -=loan
        loan = 0
        document.getElementById("bankBalance").innerHTML = bankBalance;
        payBackLoanButton.style.visibility = "hidden"
    }
    else {
        loan -= bankBalance
        bankBalance=0
        document.getElementById("bankBalance").innerHTML = bankBalance;
    }
  });

  fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
  .then(response => response.json())
  .then(data=>laptops = data)
  .then(laptops => addLaptopsToMenu(laptops))

  const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))
  } 

  const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    selectLaptops.appendChild(laptopElement)
  } 

